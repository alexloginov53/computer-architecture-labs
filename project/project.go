package main

import (
	"fmt"
	"strings"
	"os"
	"path/filepath"
	)
	
var (
	path_map map[string][]int
	result_map map[string]bool
	)
	
func main () {
    if len(os.Args) == 1 || os.Args[1] == "-h" ||
        os.Args[1] == "--help" || len(os.Args) > 3 {
        fmt.Printf("usage: %s <string> <template-substring> \n",
					filepath.Base(os.Args[0]))
        os.Exit(1)
    }
	
	ok, result := Search_substrings(os.Args[1], os.Args[2])
	if !ok {
		fmt.Printf("<template-substring> can't be empty")
		os.Exit(2)
	}
	fmt.Printf("Parsed substrings: \n")
	for i, substr := range result {
		fmt.Printf("%v: %v \n", i, substr)
	}
}

func Search_substrings (str string, substr string) (bool, []string) {
	path_map = make(map[string][]int)
	result_map = make(map[string]bool)
	f := func(c rune) bool {
		return c == '*'
	}
	arr := strings.FieldsFunc(substr, f)
	if substr == "" {
		return false, []string{}
	}
	if strings.HasPrefix(substr, "*") && len(arr) == 0 {
		collect (str, "", "")
		collect ("", "", str)
		return true, result_map_to_array()
	}
	start_search (str, arr, strings.HasPrefix(substr, "*"), strings.HasSuffix(substr, "*"))
	return true, result_map_to_array()
}

func fill_map (str string, arr []string) [][]int {
	path := [][]int{}
	for _, i:= range arr {
		find_every(str, i)
		path = append(path, path_map[i])
	}
	return path
}

func result_map_to_array () []string {
	var keys []string
	for k := range result_map {
		keys = append(keys, k)
	}
	return keys 
}

func push_to_result (str string) {
	result_map[str] = true
}

func find_every (str string, substr string) {
	if _, ok := path_map[substr]; !ok {
    	tmp := strings.Index(str, substr)
		if tmp != -1 {
			arr := []int{}
			shift := 0
			//sublen := len(substr)
			for tmp != -1 {
				arr = append (arr, shift + tmp)
				shift = shift + tmp + 1
				str = str[tmp + 1:]
				tmp = strings.Index(str, substr)
			}
			path_map[substr] = arr
		}
	} 
}

func start_search (str string, arr []string, start bool, rest bool) {
	path := fill_map(str, arr)
	if len(path[0]) != 0 {
		if len(arr) == 1 {
			for _, first_val := range path[0] {
				collect(get_start_str(start, str, first_val), 
							arr[0], 
							get_rest_str(rest, str, first_val+len(arr[0])))
			}
		} else {
		    /*if start {
				path[0] = []int{path[0][0]}
			}
			if rest {
				path[len(path)-1] = []int{path[len(path)-1][len(path[len(path)-1])-1]}
			}*/
			for _, first_val := range path[0] {
				result, end_arr := traverse(path[1:], arr[1:], first_val + len(arr[0]))
				if result {
					end_len := len(arr[len(arr) - 1])
					for _, end_val := range end_arr{
						result_str := str[first_val:end_val+end_len]
						collect(get_start_str(start, str, first_val), 
								result_str, 
								get_rest_str(rest, str, end_val+end_len))
				} 
			} else {break}
		}
		}
	}
}

func get_start_str (start bool, str string, val int) string {
	start_str := ""
	if start {
		start_str = str[:val]
	}
	return start_str
}

func get_rest_str (rest bool, str string, val int) string {
	rest_str := ""
	if rest { 
		rest_str = str[val:]
	}
	return rest_str
}

func collect (start string, body string, end string) {
	for i := 0; i <= len(start); i++ {
		for j := 0; j <= len(end); j++ {
			res := start[i:] + body + end[:j]
			if len(res) > 0 {
				push_to_result(res)
			}
		}
	}
}

func traverse (path [][]int, arr []string, prev int) (bool, []int) {
	var tmp int
	for i := range arr {
		tmp = -1
		for j, val:= range path[i]{
			if val >= prev {
				tmp = j
				prev = val + len(arr[i])
				break
			}
		}
		if tmp == -1 {return false, []int{}}
	}
	return true, path[len(arr) - 1][tmp:]
}