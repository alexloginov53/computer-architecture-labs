package main

import (
	"testing"
	)

type testcase struct {
	str string
	template string
	substrings []string
}

var (
	tests = []testcase {
		{ "test11", "test1", []string{"test1"}},
		{ "test", "test", []string{"test"}},
		{ "te111testst", "te*st", []string{"te111testst", "test", "testst", "te111test"}},
		{ "te111testst", "111*t", []string{"111testst", "111t", "111test"}},
		{ "te111testst", "te*1", []string{"te111", "te11", "te1"}},
		{ "te111testst", "*11", []string{"te111", "e111", "111", "te11", "e11", "11"}},
		{ "te111testst", "1*ts", []string{"111tests", "11tests", "1tests"}},
		{ "123", "", []string{}},
		{ "123", "*", []string{"123", "23", "3", "1", "12"}},
		{ "12345", "*23*", []string{"2345", "123", "1234", "12345", "23", "234"}},
		{ "123", "****", []string{"123", "23", "3", "1", "12"}},
		{ "123", "456", []string{}},
		}
	)

func TestSearchSubstrings1(t *testing.T) {
    for _, val := range tests {
		_ ,result := Search_substrings(val.str, val.template)
		expected := val.substrings 
		if len(result) == len(expected) {
			for _, substr := range val.substrings {
				if result_map[substr] == false {
					t.Error("For", val.str, val.template,
							"expected", expected,
							"got", result)
				}
			}
		} else {
			t.Error("For", val.str, val.template,
					"expected", expected,
					"got", result, 
					"(order is not impotant)")
		}
	}
}